<?php
header('Content-type: text/html; charset=UTF-8');
class Api{
    private $errors;
    public function init(){

        if ( !isset( $_GET['method'] ) && !isset( $_GET['string'] ) ) {
            $this->index();
            exit(0);
        }

        $method = '';
        $string = '';
        if ( isset( $_GET['method'] ) ) {
            $method = $_GET['method'];
        }
        if ( isset( $_GET['string'] ) ) {
            $string = $_GET['string'];
        }

        if ( empty( $method ) ) {
            $this->errors = ["type" => "method", "textError" => "Задано пустое значение для метода" ];
        }
        if ( empty( $string ) ) {
            $this->errors = ["type" => "string", "textError" => "Задано пустое значение для строки" ];
        }
        if ( !method_exists( $this, $method ) ) {
            $this->errors = ["type" => "method", "textError" => "Метод не найден" ];
        }

        if ( !empty( $this->errors ) ) {
            echo json_encode( [
                "status" => "fail",
                "errors" => $this->errors
            ], JSON_UNESCAPED_UNICODE );
            exit(0);
        }

        $result = $this->$method( $string );

        
        echo json_encode( [
            "method" => $method,
            "status" => "success",
            "result" => $result
        ], JSON_UNESCAPED_UNICODE );
    }

    public function index(){
        $random_method = [
            "countS",
            "palindrom"
        ];
        $method = $random_method[array_rand($random_method)];
        $random_string = [
            "abccdceffgihhhhj",
            "оно",
            "Madam",
            "Saippuakivikauppias"
        ];
        $string = $random_string[array_rand($random_string)];
        $data = [
            "info" => "Для передачи данных необходимо передать 2 параметра через GET (method, string)",
            "method" => "Параметр method может принимать 2 значения (countS - для нахождения второго по встречаемости символа в строке; palindrom - для определения является ли строка палиндромом)",
            "string" => "Входящая строка",
            "example" => "<a href='?method=".$method."&string=".$string."'>?method=".$method."&string=".$string."</a>",
        ];
        echo "<pre>";
        echo json_encode( $data, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT );
        echo "</pre>";
        exit(0);
    }


    function countS( $string = "" ){
        $data = [];
        foreach (count_chars($string, 1) as $key => $value) {
            $data[ $key ] = $value;
        }
        $maxs = array_keys($data, max($data));
        unset( $data[ $maxs[0] ] );
        
        $maxs = array_keys($data, max($data));
        return chr( $maxs[0] ) ;
    }
    function mb_str_split($string) { 
        return preg_split('#(?<!^)(?!$)#u', $string); 
    } 
    function palindrom( $string = "" ){
        $string = str_replace(' ','',$string);
        $data = $this->mb_str_split( mb_strtolower($string) );
        $l = count($data) -1;
        for ($i=0; $i < $l; $i++) { 
            if ( $data[$i] != $data[$l-$i]) {
                return false;
            }
        }
        return true;
    }
}

$api = new Api();
$api->init();

/*
$string = "abccdceffgihhhhj";
$s = countS( $string );
echo $s;
echo "<br>";

$string2 = "оно";
$res = palindrom( $string2 );
var_dump( $res );
echo "<br>";
$string2 = "Madam";
$res = palindrom( $string2 );
var_dump( $res );
echo "<br>";
$string2 = "Saippuakivikauppias ";
$res = palindrom( $string2 );
var_dump( $res );
echo "<br>";
*/